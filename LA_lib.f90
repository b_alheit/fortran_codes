
subroutine  mult_matricies(mat_A, mat_B, a_rows, a_cols_b_rows, b_cols, out)
  integer :: a_rows, a_cols_b_rows, b_cols
  real:: mat_A(a_rows, a_cols_b_rows), mat_B(a_cols_b_rows, b_cols)
  real, dimension(a_rows,b_cols) :: out
  integer :: i,j,k

  out = 0

  do i = 1,a_rows
    do j = 1,a_cols_b_rows
      do k = 1,b_cols
        out(i,k) = out(i,k) + mat_A(i,j) * mat_B(j,k)
      enddo
    enddo
  enddo
end

subroutine  tensor_product_2(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  real:: mat_A(len_a, 1), mat_B(len_b, 1)
  real, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i,1) * mat_B(j,1)
    enddo
  enddo

end

subroutine  tensor_product_1(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  real:: mat_A(len_a), mat_B(len_b)
  real, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i) * mat_B(j)
    enddo
  enddo

end

subroutine print_matrix(mat, rows, collumns)
  integer :: rows, collumns
  real :: mat(rows, collumns)
  integer :: i

  do i =1,rows
    print*,mat(i,:)
  enddo
end
