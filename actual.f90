program xfunc
   implicit none
   integer, parameter :: p = 1
   integer, parameter :: q = 2

   real :: arr1(2, 1), arr2(1, 2)
   real, dimension(2,2) :: out, tens
   ! real, dimension(2,2) :: mult_matricies
   arr1(:, 1) = [1, 5]
   arr2(1, :) = [1, 2]
   ! print*,"arr1 + arr2",arr1 * arr2
   call mult_matricies(arr1, arr2, 2, 1, 2, out)
   print*,"len(shape(arr1))",shape(shape(arr1))
   call print_matrix(out, 2, 2)
   ! print*,"mult_matricies(arr1, arr2)",out(1,:)
   ! print*,"mult_matricies(arr1, arr2)",out(2,:)
   ! print*,"arr1 + arr2",arr1 * arr2
   call tensor_product_2(arr1, arr1, 2, 2, tens)
   print*,
   print*,arr1 *2
   print*,
   call print_matrix(tens, 2, 2)
end program xfunc
